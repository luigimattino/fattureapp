class UserModel {
  String sex;
  String fullName;
  String firstRowAddress;
  String secondRowAddress;
  String partitaIvaNumber;
  String fiscalCodeNumber;
  String alboNumber;
  String bankName;
  String bankIban;

  UserModel(
      {this.sex,
      this.fullName,
      this.firstRowAddress,
      this.secondRowAddress,
      this.partitaIvaNumber,
      this.fiscalCodeNumber,
      this.alboNumber,
      this.bankName,
      this.bankIban});

  Map<String, dynamic> toJson() => {
        'sex': sex,
        'fullName': fullName,
        'firstRowAddress': firstRowAddress,
        'secondRowAddress': secondRowAddress,
        'partitaIvaNumber': partitaIvaNumber,
        'fiscalCodeNumber': fiscalCodeNumber,
        'alboNumber': alboNumber,
        'bankName': bankName,
        'bankIban': bankIban
      };

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        sex: json['sex'],
        fullName: json['fullName'],
        firstRowAddress: json['firstRowAddress'],
        secondRowAddress: json['secondRowAddress'],
        partitaIvaNumber: json['partitaIvaNumber'],
        fiscalCodeNumber: json['fiscalCodeNumber'],
        alboNumber: json['alboNumber'],
        bankName: json['bankName'],
        bankIban: json['bankIban']);
  }
}
