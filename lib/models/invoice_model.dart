class InvoicesList {
  final List<InvoiceModel> invoices;

  InvoicesList({
    this.invoices,
  });

  int length() {
    if (invoices != null) return invoices.length;
    return 0;
  }

  factory InvoicesList.fromJson(List<dynamic> parsedJson) {
    List<InvoiceModel> invoices = new List<InvoiceModel>();
    invoices = parsedJson.map((i) => InvoiceModel.fromJson(i)).toList();

    return new InvoicesList(invoices: invoices);
  }
}

class RecipientModel {
  String genre;
  String fullname;
  String firstRowAddress;
  String secondRowAddress;
  String fiscalCodeNumber;

  RecipientModel({
    this.genre,
    this.fullname,
    this.firstRowAddress,
    this.secondRowAddress,
    this.fiscalCodeNumber,
  });

  Map<String, dynamic> toJson() => {
        'genre': genre,
        'fullname': fullname,
        'firstRowAddress': firstRowAddress,
        'secondRowAddress': secondRowAddress,
        'fiscalCodeNumber': fiscalCodeNumber,
      };

  factory RecipientModel.fromJson(Map<String, dynamic> json) {
    return RecipientModel(
        genre: json['genre'],
        fullname: json['fullname'],
        firstRowAddress: json['firstRowAddress'],
        secondRowAddress: json['secondRowAddress'],
        fiscalCodeNumber: json['fiscalCodeNumber']);
  }
}

class InvoiceModel {
  static final double taxPercentage = 2.0;
  static final double stampDutyMinimumLimit = 77.47;
  String id;
  int number;
  RecipientModel recipient;
  DateTime timestamp;
  String itemLabel;
  double amountBT;
  double taxAmount;
  double amountAT;

  InvoiceModel(
      {this.id,
      this.number,
      this.recipient,
      this.itemLabel,
      this.amountBT,
      this.taxAmount,
      this.timestamp,
      this.amountAT});

  Map<String, dynamic> toJson() => {
        'id': id,
        'number': number,
        'recipient': recipient,
        'timestamp': timestamp.toIso8601String(),
        'itemLabel': itemLabel,
        'amountBT': amountBT,
        'taxAmount': taxAmount,
        'amountAT': amountAT
      };

  factory InvoiceModel.fromJson(Map<String, dynamic> json) {
    return InvoiceModel(
        id: json['id'],
        number: json['number'],
        recipient: RecipientModel.fromJson(json['recipient']),
        timestamp: DateTime.parse(json['timestamp']),
        itemLabel: json['itemLabel'],
        amountBT: json['amountBT'],
        taxAmount: json['taxAmount'],
        amountAT: json['amountAT']);
  }
}
