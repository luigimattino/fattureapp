import 'package:FattureApp/app_theme.dart';
import 'package:flutter/material.dart';

class ResultDialog extends StatelessWidget {
  final String title, description, buttonText;
  final double taxPercentage, taxAmount, amountAT;
  final bool stampDutyMinimumLimit;

  ResultDialog(
      {@required this.title,
      @required this.description,
      @required this.buttonText,
      @required this.taxPercentage,
      @required this.taxAmount,
      @required this.amountAT,
      this.stampDutyMinimumLimit = false});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      elevation: 10.0,
      backgroundColor: Colors.transparent,
      child: _dialogContent(context),
    );
  }

  _dialogContent(BuildContext context) {
    final rows = <TableRow>[];
    rows.add(TableRow(children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "tasse ${taxPercentage}%",
          textAlign: TextAlign.start,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          " + € ${taxAmount.toStringAsFixed(2)}",
          textAlign: TextAlign.end,
        ),
      ),
    ]));
    if (stampDutyMinimumLimit) {
      rows.add(TableRow(children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "L'imposta di bollo",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            " + € 2.00",
            textAlign: TextAlign.end,
          ),
        ),
      ]));
    }
    rows.add(TableRow(children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "Totale",
          textScaleFactor: 1.5,
          textAlign: TextAlign.start,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          " € ${amountAT.toStringAsFixed(2)}",
          textScaleFactor: 1.5,
          textAlign: TextAlign.end,
        ),
      ),
    ]));

    return Container(
      height: 300,
      decoration: BoxDecoration(
          color: AppTheme.bgColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10.0)),
      child: Column(
        children: <Widget>[
          SizedBox(height: 4.0),
          Text(
            title,
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 4.0),
          Text(
            description,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Table(
              border: TableBorder.all(width: 1.0, color: Colors.grey),
              children: rows,
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).pop(); // To close the dialog
              },
              child: Text(buttonText),
            ),
          ),
        ],
      ),
    );
  }
}
