import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static const Color primaryColor = Color(0xFF7E1946);
  static const Color secondaryColor = Color(0xFF06314B);
  static const Color bgColor = Color(0xFFFBFEF9);
  static const Color primaryTextColor = Color(0xFF000004);
  static const Color secondaryTextColor = Color(0xFFA63446);
  static const Color tertiaryTextColor = Color(0xFFD199A0);
  static const TextStyle alertTextStyle =
      TextStyle(color: Colors.black, fontSize: 16, fontFamily: 'OpenSans');

  static final ThemeData theme = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.light,
    primaryColor: primaryColor,
    accentColor: secondaryColor,
    buttonTheme: ButtonThemeData(
      buttonColor: primaryColor,
      textTheme: ButtonTextTheme.primary,
    ),
    textTheme: _textTheme,
    // Define the default font family.
    fontFamily: 'OpenSans',
  );

  static final TextTheme _textTheme = TextTheme(
    button: _buttonTextStyle,
  );
  static final TextStyle _buttonTextStyle =
      TextStyle(fontSize: 16.0, letterSpacing: 1.2, color: secondaryTextColor);
}
