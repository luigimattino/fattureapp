import 'dart:async';

import 'package:FattureApp/bloc/user_bloc.dart';
import 'package:FattureApp/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rxdart/rxdart.dart';

import '../app_routes.dart';
import '../app_theme.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  final UserBloc _userBloc = new UserBloc();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Map<int, TextEditingController> controllers = {};
  String _sex_value = 'M';

  @override
  void initState() {
    _userBloc.fetchData();
    super.initState();
  }

  @override
  void dispose() {
    _userBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    RangeStream(0, 7).listen((index) {
      controllers.putIfAbsent(index, () => TextEditingController());
    });
    return Scaffold(
      appBar: AppBar(
        title: Text('Utente'),
        actions: [
          IconButton(
            icon: Icon(
              FontAwesomeIcons.home,
              color: AppTheme.tertiaryTextColor,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(
                context,
                AppRoutes.HomeRoute,
              );
            },
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Text(
                'Dati necessari per la fatturazione',
                style: TextStyle(fontSize: 16),
              ),
              StreamBuilder(
                stream: _userBloc.userObservable,
                builder: (context, AsyncSnapshot<UserModel> snapshot) {
                  if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  }
                  if (snapshot.connectionState != ConnectionState.waiting) {
                    if (snapshot.hasData) {
                      if (snapshot.data.sex != null) {
                        _sex_value = snapshot.data.sex;
                      }

                      controllers.forEach((index, controller) {
                        controller.text = extractText(index, snapshot.data);
                      });
                    }
                    return _buildForm(snapshot.data);
                  }
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      height: 150,
                      child: Center(child: CircularProgressIndicator()));
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildForm(UserModel data) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text((() {
          if (data != null)
            return 'modifica per variare i tuoi dati';
          else
            return 'inserisci tutti i tuoi dati';
        })()),
        Form(
          key: _formKey,
          child: Column(
            children: [
              DropdownButton(
                value: _sex_value,
                items: [
                  DropdownMenuItem(
                    child: Text("Dott."),
                    value: 'M',
                  ),
                  DropdownMenuItem(
                    child: Text("Dott.ssa"),
                    value: 'F',
                  ),
                ],
                onChanged: (value) {
                  setState(() {
                    _sex_value = value;
                  });
                },
              ),
              ...List.generate(8, (index) {
                var controller = controllers[index];
                Widget textFormField;
                switch (index) {
                  case 0:
                    textFormField = _buildFullNameTextField(controller);
                    break;
                  case 1:
                    textFormField = _buildFirstRowAddressTextField(controller);
                    break;
                  case 2:
                    textFormField = _buildSecondRowAddressTextField(controller);
                    break;
                  case 3:
                    textFormField = _buildPartitaIvaNumberTextField(controller);
                    break;
                  case 4:
                    textFormField = _buildFiscalCodeNumberTextField(controller);
                    break;
                  case 5:
                    textFormField = _buildAlboNumberTextField(controller);
                    break;
                  case 6:
                    textFormField = _buildBankNameTextField(controller);
                    break;
                  case 7:
                    textFormField = _buildBankIbanTextField(controller);
                    break;
                }
                return Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 5,
                  ),
                  child: textFormField,
                );
              }),
              Container(
                padding: EdgeInsets.fromLTRB(10, 35, 10, 20),
                child: _buildButtons(data),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _buildButtons(UserModel data) {
    if (data != null)
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(minWidth: 200),
            child: RaisedButton(
              elevation: 5,
              padding: EdgeInsets.all(5.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              onPressed: () async {
                _userBloc.saveData(UserModel(
                  sex: _sex_value,
                  fullName: controllers[0].text,
                  firstRowAddress: controllers[1].text,
                  secondRowAddress: controllers[2].text,
                  partitaIvaNumber: controllers[3].text,
                  fiscalCodeNumber: controllers[4].text,
                  alboNumber: controllers[5].text,
                  bankName: controllers[6].text,
                  bankIban: controllers[7].text,
                ));
                setState(() {});
                _userBloc.fetchData();
              },
              child: Text('Salva'),
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(minWidth: 130),
            child: RaisedButton(
              elevation: 5,
              padding: EdgeInsets.all(5.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              onPressed: () async {
                _userBloc.deleteData();

                controllers.forEach((index, controller) {
                  controller.text = "";
                });
                _userBloc.fetchData();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Elimina'),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                    child: Icon(
                      FontAwesomeIcons.trash,
                      size: 14,
                    ),
                  ),
                ],
              ),
              color: AppTheme.secondaryColor,
            ),
          ),
        ],
      );
    else
      return SizedBox(
        width: double.infinity,
        child: RaisedButton(
          elevation: 5,
          padding: EdgeInsets.all(5.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          onPressed: () async {
            _userBloc.saveData(UserModel(
              sex: _sex_value,
              fullName: controllers[0].text,
              firstRowAddress: controllers[1].text,
              secondRowAddress: controllers[2].text,
              partitaIvaNumber: controllers[3].text,
              fiscalCodeNumber: controllers[4].text,
              alboNumber: controllers[5].text,
              bankName: controllers[6].text,
              bankIban: controllers[7].text,
            ));
            setState(() {});
            _userBloc.fetchData();
          },
          child: Text('Save'),
        ),
      );
  }

  Widget _buildFullNameTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        hintText: 'Nome Completo',
        labelText: (_sex_value == 'M') ? 'Dott.' : 'Dott.ssa',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildFirstRowAddressTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        hintText: 'Indirizzo (prima riga)',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildSecondRowAddressTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        hintText: 'Indirizzo (seconda riga)',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildPartitaIvaNumberTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      textCapitalization: TextCapitalization.characters,
      decoration: InputDecoration(
        hintText: 'numero P.Iva',
        labelText: 'P.IVA',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildFiscalCodeNumberTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      textCapitalization: TextCapitalization.characters,
      decoration: InputDecoration(
        hintText: 'Codice Fiscale',
        labelText: 'C.F.',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildAlboNumberTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        hintText: 'Numero iscrizione Albo Psicologi',
        labelText: 'N° iscrizione Albo Psic.',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildBankNameTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        hintText: 'Intestazione banca',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  Widget _buildBankIbanTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      textCapitalization: TextCapitalization.characters,
      decoration: InputDecoration(
        hintText: 'IBAN',
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'il campo è obbligatorio';
        }
      },
    );
  }

  String extractText(int index, UserModel data) {
    String text;
    switch (index) {
      case 0:
        text = data.fullName;
        break;
      case 1:
        text = data.firstRowAddress;
        break;
      case 2:
        text = data.secondRowAddress;
        break;
      case 3:
        text = data.partitaIvaNumber;
        break;
      case 4:
        text = data.fiscalCodeNumber;
        break;
      case 5:
        text = data.alboNumber;
        break;
      case 6:
        text = data.bankName;
        break;
      case 7:
        text = data.bankIban;
        break;
    }
    return text;
  }
}
