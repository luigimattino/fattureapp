import 'package:flutter/foundation.dart';
import '../repository/i_local_storage_repository.dart';
import 'local_storage_service.dart';

class NumberSequenceService {
  final LocalStorageService _localStorageService;
  final String _sequenceKey = "sequence";

  NumberSequenceService({
    @required ILocalStorageRepository localStorageRepository,
  }) : _localStorageService =
            LocalStorageService(localStorageRepository: localStorageRepository);

  Future<int> getSequence() async {
    return await _localStorageService.getAll(_sequenceKey) ?? 1;
  }

  Future<int> incrementAndSave(int sequence) async {
    sequence += 1;
    await _localStorageService.save(_sequenceKey, sequence);

    return sequence;
  }

  Future<int> decrementAndSave(int sequence) async {
    sequence -= 1;
    await _localStorageService.save(_sequenceKey, sequence);

    return sequence;
  }
}
