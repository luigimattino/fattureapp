import 'package:flutter/foundation.dart';
import '../models/invoice_model.dart';
import '../repository/i_local_storage_repository.dart';
import 'local_storage_service.dart';

class InvoiceService {
  final LocalStorageService _localStorageService;
  final String _invoicesKey = "invoices";

  InvoiceService({
    @required ILocalStorageRepository localStorageRepository,
  }) : _localStorageService =
            LocalStorageService(localStorageRepository: localStorageRepository);

  Future<InvoicesList> getInvoicesFromCache() async {
    List<dynamic> data = await _localStorageService.getItem(_invoicesKey);
    if (data == null) {
      return null;
    }
    InvoicesList list = InvoicesList.fromJson(data);
    return list;
  }

  void saveNew(InvoiceModel invoice) async {
    List<dynamic> data = await _localStorageService.getItem(_invoicesKey);
    InvoicesList list;
    if (data != null) {
      list = InvoicesList.fromJson(data);
    } else {
      list = InvoicesList.fromJson([]);
    }
    list.invoices.add(invoice);
    _localStorageService.save(_invoicesKey, list.invoices);
  }

  Future<InvoicesList> delete(InvoiceModel invoice) async {
    List<dynamic> data = await _localStorageService.getItem(_invoicesKey);
    if (data == null) {
      return null;
    }
    InvoicesList list = InvoicesList.fromJson(data);
    list = InvoicesList(
        invoices: list.invoices
            .where((element) => (element.number != invoice.number))
            .toList());
    //list.invoices;
    return _localStorageService
        .save(_invoicesKey, list.invoices)
        .then((_) => list);
  }

  Future<InvoicesList> search(String text) async {
    List<dynamic> data = await _localStorageService.getItem(_invoicesKey);
    if (data == null) {
      return null;
    }
    InvoicesList list = InvoicesList.fromJson(data);
    bool Function(InvoiceModel) check;
    if (int.tryParse(text) != null) {
      check = (a) => (a.number == int.parse(text));
    } else if (text == null || text.isEmpty) {
      check = (a) => true;
    } else {
      check = (a) => (a.recipient.fullname.contains(text));
    }
    list = InvoicesList(invoices: list.invoices.where(check).toList());
    //list.invoices;
    return list;
  }

  void deleteAll() async {
    _localStorageService.delete(_invoicesKey);
  }
}
