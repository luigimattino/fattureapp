import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import '../app_routes.dart';

class PdfPreviewScreen extends StatelessWidget {
  final String path;

  PdfPreviewScreen({this.path});

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      appBar: AppBar(
        title: Text("Parcella"),
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.home),
            onPressed: () {
              Navigator.pushReplacementNamed(
                context,
                AppRoutes.HomeRoute,
              );
            },
          ),
          IconButton(
            icon: Icon(FontAwesomeIcons.share),
            onPressed: () async {
              try {
                File pdfFile = File(path);

                //final ByteData bytes = await fileBundle.load(path);
                await Share.file('Fattura', 'fattura.pdf',
                    pdfFile.readAsBytesSync(), 'application/pdf',
                    text: 'My optional text.');
              } catch (e) {
                print('error: $e');
              }
            },
          ),
        ],
      ),
      path: path,
    );
  }
}
