import 'package:FattureApp/models/invoice_model.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';
import '../widgets/result_dialog.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../services/number_sequence_service.dart';
import '../repository/local_storage_repository.dart';
import '../services/invoice_service.dart';
import 'dart:math' as math;
import '../app_routes.dart';
import '../app_theme.dart';

class InvoiceScreen extends StatefulWidget {
  InvoiceModel copyInvoice;
  InvoiceScreen({Key key, this.copyInvoice}) : super(key: key);

  @override
  _InvoiceScreenState createState() => _InvoiceScreenState();
}

class _InvoiceScreenState extends State<InvoiceScreen> {
  bool complete = false;
  final RegExp expCurrencyZero = new RegExp(r'^(0*)(\.|\.0+)?$');
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  DateFormat dateformat = DateFormat('dd/MM/yyyy');
  var uuid = new Uuid(options: {'grng': UuidUtil.cryptoRNG});
  Map<int, TextEditingController> controllers = {};
  String _genre_value = 'M';
  InvoiceService _invoiceService;
  NumberSequenceService _numberSequenceService;

  @override
  void initState() {
    if (widget.copyInvoice != null) {
      _genre_value = widget.copyInvoice.recipient.genre;
      controllers = {
        0: TextEditingController(text: widget.copyInvoice.recipient.fullname),
        1: TextEditingController(
            text: widget.copyInvoice.recipient.firstRowAddress),
        2: TextEditingController(
            text: widget.copyInvoice.recipient.secondRowAddress),
        3: TextEditingController(
            text: widget.copyInvoice.recipient.fiscalCodeNumber),
        4: TextEditingController(text: widget.copyInvoice.itemLabel),
        5: TextEditingController(text: widget.copyInvoice.amountBT.toString()),
        6: TextEditingController(),
        7: TextEditingController(text: dateformat.format(DateTime.now())),
      };
    } else {
      controllers = {
        0: TextEditingController(),
        1: TextEditingController(),
        2: TextEditingController(),
        3: TextEditingController(),
        4: TextEditingController(),
        5: TextEditingController(),
        6: TextEditingController(),
        7: TextEditingController(text: dateformat.format(DateTime.now())),
      };
    }
    _invoiceService = InvoiceService(
        localStorageRepository: LocalStorageRepository("invoices.json"));
    _numberSequenceService = NumberSequenceService(
        localStorageRepository: LocalStorageRepository("number_sequence.json"));

    _numberSequenceService.getSequence().asStream().listen((seq_num) {
      controllers[6].text = seq_num.toString();
    });
    super.initState();
  }

  @override
  void setState(fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    //final Map<String, dynamic> args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Nuova Fattura'),
        actions: [
          IconButton(
            icon: Icon(
              FontAwesomeIcons.times,
              color: AppTheme.tertiaryTextColor,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(
                context,
                AppRoutes.HomeRoute,
              );
            },
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Column(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: _boxDecoration(),
                      child: _buildDataForm(),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: _boxDecoration(),
                      child: _buildRecipientForm(),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: _boxDecoration(),
                      child: _buildAmountForm(),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 35, 10, 20),
                      child: _buildButtons(),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(minWidth: 200),
          child: RaisedButton(
            padding: EdgeInsets.all(5.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            onPressed: () async {
              if (!_formKey.currentState.validate()) {
                return;
              }

              int number = int.parse(controllers[6].text);
              _numberSequenceService.incrementAndSave(number);

              RecipientModel recipient = RecipientModel(
                genre: _genre_value,
                fullname: controllers[0].text,
                firstRowAddress: controllers[1].text,
                secondRowAddress: controllers[2].text,
                fiscalCodeNumber: controllers[3].text,
              );

              InvoiceModel invoice = InvoiceModel(
                  id: uuid.v4(),
                  number: number,
                  recipient: recipient,
                  itemLabel: controllers[4].text,
                  amountBT: double.parse(controllers[5].text),
                  timestamp: dateformat.parse(controllers[7].text));
              double taxAmount =
                  (InvoiceModel.taxPercentage / 100) * invoice.amountBT;
              taxAmount = _roundDouble(taxAmount, 2);
              invoice.taxAmount = taxAmount;
              double amountAT = invoice.amountBT + invoice.taxAmount;
              amountAT +=
                  (invoice.amountBT > InvoiceModel.stampDutyMinimumLimit)
                      ? 2.0
                      : 0.0;
              invoice.amountAT = _roundDouble(amountAT, 2);
              _invoiceService.saveNew(invoice);
              Navigator.pushReplacementNamed(
                context,
                AppRoutes.HomeRoute,
              );
            },
            child: Text('Salva'),
          ),
        ),
        ConstrainedBox(
          constraints: BoxConstraints(minWidth: 130),
          child: RaisedButton(
            padding: EdgeInsets.all(5.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            onPressed: () {
              if (!_formKey.currentState.validate()) {
                return;
              }
              double amountBT = double.parse(controllers[5].text);
              double taxAmount = (InvoiceModel.taxPercentage / 100) * amountBT;
              taxAmount = _roundDouble(taxAmount, 2);
              double amountAT = amountBT + taxAmount;
              bool isStampDutyMinimumLimit =
                  (amountBT > InvoiceModel.stampDutyMinimumLimit);
              if (isStampDutyMinimumLimit) {
                amountAT += 2.0;
              }
              amountAT = _roundDouble(amountAT, 2);
              showDialog(
                context: context,
                builder: (BuildContext context) => ResultDialog(
                  title: "Calcolo totale",
                  description: "sulla parcella di € ${amountBT}",
                  buttonText: "Chiudi",
                  amountAT: amountAT,
                  taxAmount: taxAmount,
                  taxPercentage: InvoiceModel.taxPercentage,
                  stampDutyMinimumLimit: isStampDutyMinimumLimit,
                ),
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text('Calcola'),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                  child: Icon(
                    FontAwesomeIcons.calculator,
                    size: 14,
                  ),
                ),
              ],
            ),
            color: AppTheme.secondaryColor,
          ),
        )
      ],
    );
  }

  BoxDecoration _boxDecoration() {
    return BoxDecoration(
      color: AppTheme.bgColor,
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ],
      border: Border.all(width: 1.0, color: Colors.grey[200]),
      borderRadius: BorderRadius.all(
          Radius.circular(5.0) //         <--- border radius here
          ),
    );
  }

  Widget _buildDataForm() {
    return Container(
      child: Column(
        children: <Widget>[
          Text('Intestazione'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: controllers[6],
                    decoration: InputDecoration(labelText: 'Numero'),
                    textAlign: TextAlign.end,
                    keyboardType: TextInputType.number,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'il campo è obbligatorio';
                      }
                    },
                  ),
                ),
              ),
              Flexible(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: controllers[7],
                    decoration: InputDecoration(labelText: 'Data di Fattura'),
                    keyboardType: TextInputType.datetime,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'il campo è obbligatorio';
                      }
                    },
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildRecipientForm() {
    return Container(
      child: Column(
        children: <Widget>[
          Text('Destinatario'),
          DropdownButton(
            value: _genre_value,
            items: [
              DropdownMenuItem(
                child: Text("Sig."),
                value: 'M',
              ),
              DropdownMenuItem(
                child: Text("Sig.ra"),
                value: 'F',
              ),
              DropdownMenuItem(
                child: Text("Genitori"),
                value: 'G',
              ),
            ],
            onChanged: (value) {
              setState(() {
                _genre_value = value;
              });
            },
          ),
          TextFormField(
            controller: controllers[0],
            decoration: InputDecoration(labelText: 'Nome Completo'),
            validator: (String value) {
              if (value.isEmpty) {
                return 'il campo è obbligatorio';
              }
            },
          ),
          TextFormField(
            controller: controllers[1],
            decoration: InputDecoration(labelText: 'Indirizzo (prima riga)'),
            validator: (String value) {
              if (value.isEmpty) {
                return 'il campo è obbligatorio';
              }
            },
          ),
          TextFormField(
            controller: controllers[2],
            decoration: InputDecoration(labelText: 'Indirizzo (seconda riga)'),
            validator: (String value) {
              if (value.isEmpty) {
                return 'il campo è obbligatorio';
              }
            },
          ),
          TextFormField(
            controller: controllers[3],
            decoration: InputDecoration(labelText: 'Codice Fiscale'),
            textCapitalization: TextCapitalization.characters,
            validator: (String value) {
              if (value.isEmpty) {
                return 'il campo è obbligatorio';
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildAmountForm() {
    return Container(
      child: Column(
        children: <Widget>[
          Text('Parcella'),
          TextFormField(
            controller: controllers[4],
            decoration:
                InputDecoration(labelText: 'Descrizione della prestazione'),
            validator: (String value) {
              if (value.isEmpty) {
                return 'il campo è obbligatorio';
              }
            },
          ),
          TextFormField(
            controller: controllers[5],
            textAlign: TextAlign.end,
            decoration: InputDecoration(
              suffixIcon: Icon(
                FontAwesomeIcons.euroSign,
                color: AppTheme.primaryTextColor,
                size: 24,
              ),
              labelText: 'Importo',
            ),
            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
            keyboardType: TextInputType.numberWithOptions(decimal: true),
            validator: (String value) {
              bool hasMatch = expCurrencyZero.hasMatch(value);
              if (value.isEmpty || hasMatch) {
                return 'L\'importo è obbligatorio';
              }
            },
          ),
        ],
      ),
    );
  }

  double _roundDouble(double value, int places) {
    double mod = math.pow(10.0, places);
    return ((value * mod).round().toDouble() / mod);
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange})
      : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;
  final RegExp expCurrency =
      new RegExp(r'^(0|([1-9][0-9]*))(\.|\.[0-9]{1,2})?$');

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      bool hasMatch = expCurrency.hasMatch(value);
      if (value.isNotEmpty && !hasMatch) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";
        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}
