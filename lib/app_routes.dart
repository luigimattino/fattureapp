import 'package:FattureApp/screens/pdf_preview_screen.dart';
import 'package:flutter/material.dart';
import 'screens/home_screen.dart';
import 'screens/invoice_screen.dart';
import 'screens/user_screen.dart';

class AppRoutes {
  AppRoutes._();

  static const HomeRoute = '/';
  static const UserRoute = '/user';
  static const AddInvoiceScreenRoute = '/add-invoice';
  static const PdfPreviewScreenRoute = '/pdf-preview';

  static final RouteFactory routes = (settings) {
    final Map<String, dynamic> arguments = settings.arguments;
    Widget screen;
    switch (settings.name) {
      case HomeRoute:
        screen = HomeScreen();
        break;
      case UserRoute:
        screen = UserScreen();
        break;
      case AddInvoiceScreenRoute:
        if (arguments != null && arguments.containsKey('invoice_model')) {
          screen = InvoiceScreen(
            copyInvoice: arguments['invoice_model'],
          );
        } else {
          screen = InvoiceScreen();
        }
        break;
      case PdfPreviewScreenRoute:
        screen = PdfPreviewScreen(path: arguments['pdf_path']);
        break;
      default:
        return null;
    }
    return MaterialPageRoute(
      builder: (BuildContext context) => screen,
    );
  };
}
