import 'package:rxdart/rxdart.dart';
import '../models/invoice_model.dart';
import '../repository/local_storage_repository.dart';
import '../services/invoice_service.dart';

class InvoicesBloc {
  BehaviorSubject<InvoicesList> _subjectInvoices;
  InvoiceService _invoiceService;
  InvoicesBloc() {
    _invoiceService = InvoiceService(
        localStorageRepository: LocalStorageRepository("invoices.json"));
    _subjectInvoices = new BehaviorSubject<InvoicesList>();
  }

  ValueStream<InvoicesList> get invoicesObservable => _subjectInvoices.stream;

  void fetchData() {
    _invoiceService.getInvoicesFromCache().asStream().listen((data) {
      _subjectInvoices.sink.add(data);
    });
  }

  void filterData(String text) {
    _invoiceService.search(text).asStream().listen((data) {
      _subjectInvoices.sink.add(data);
    });
  }

  void delete(InvoiceModel invoice) {
    _invoiceService.delete(invoice).asStream().listen((data) {
      _subjectInvoices.sink.add(data);
    });
  }

  void dispose() {
    _subjectInvoices.close();
  }
}
