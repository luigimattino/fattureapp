import 'package:flutter/foundation.dart';
import 'package:FattureApp/models/user_model.dart';
import '../repository/i_local_storage_repository.dart';
import 'local_storage_service.dart';

class UserService {
  final LocalStorageService _localStorageService;
  final String _loginKey = "user";

  UserService({
    @required ILocalStorageRepository localStorageRepository,
  }) : _localStorageService =
            LocalStorageService(localStorageRepository: localStorageRepository);

  Future<UserModel> getUserFromCache() async {
    Map<String, dynamic> data = await _localStorageService.getItem(_loginKey);
    if (data == null) {
      return null;
    }
    UserModel login = UserModel.fromJson(data);
    return login;
  }

  void saveUser(UserModel user) async {
    _localStorageService.save(_loginKey, user);
  }

  void deleteUser() async {
    _localStorageService.delete(_loginKey);
  }
}
