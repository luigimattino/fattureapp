import 'dart:async';

import '../bloc/user_bloc.dart';

import '../services/pdf_builder_service.dart';
import 'package:intl/intl.dart';
import 'package:FattureApp/bloc/invoices_bloc.dart';
import 'package:FattureApp/models/invoice_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../app_routes.dart';
import '../app_theme.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final InvoicesBloc _invoicesBloc = new InvoicesBloc();
  final UserBloc _userBloc = new UserBloc();
  final PdfBuilderService _printService = PdfBuilderService();
  int _selectedItemIndex = -1;
  InvoiceModel _selectedItem;
  bool canAddInvoices = false;

  final Icon _searchIcon = Icon(
    FontAwesomeIcons.search,
    color: AppTheme.tertiaryTextColor,
  );
  final Icon _cancelSearchIcon = Icon(
    FontAwesomeIcons.times,
    color: AppTheme.tertiaryTextColor,
  );
  Icon currSearchIcon = Icon(FontAwesomeIcons.exclamation);
  Widget currSearchTitle = Text('Fatture');
  final _searchQuery = new TextEditingController();
  Timer _debounce;

  @override
  void initState() {
    _invoicesBloc.fetchData();
    currSearchIcon = _searchIcon;
    _searchQuery.addListener(_onSearchChanged);
    _userBloc.getUserFromCache().asStream().listen((event) {
      if (event == null) {
        _alertDialog();
      } else {
        setState(() {
          canAddInvoices = true;
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _invoicesBloc.dispose();
    _searchQuery.removeListener(_onSearchChanged);
    _searchQuery.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      print('Event search');
      _invoicesBloc.filterData(_searchQuery.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: currSearchTitle,
        actions: [
          IconButton(
            icon: currSearchIcon,
            onPressed: () {
              if (currSearchIcon.icon == FontAwesomeIcons.search) {
                setState(() {
                  currSearchIcon = _cancelSearchIcon;
                  currSearchTitle = TextField(
                    controller: _searchQuery,
                    textInputAction: TextInputAction.go,
                    style: TextStyle(
                        color: AppTheme.tertiaryTextColor, fontSize: 20.0),
                  );
                });
              } else {
                setState(() {
                  currSearchIcon = _searchIcon;
                  currSearchTitle = Text('Fatture');
                });
              }
            },
          ),
          IconButton(
            icon: Icon(
              FontAwesomeIcons.user,
              color: AppTheme.tertiaryTextColor,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(
                context,
                AppRoutes.UserRoute,
              );
            },
          )
        ],
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(10),
        child: Stack(
          children: [
            StreamBuilder(
              stream: _invoicesBloc.invoicesObservable,
              builder: (context, AsyncSnapshot<InvoicesList> snapshot) {
                if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                }
                if (snapshot.connectionState != ConnectionState.waiting) {
                  if (snapshot.hasData) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 64.0),
                      child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemCount: snapshot.data.length(),
                          itemBuilder: (BuildContext ctxt, int index) {
                            return _buildInvoiceBox(
                                snapshot.data.invoices[index], index);
                          }),
                    );
                  } else {
                    return Container();
                  }
                }
                return Container(
                    width: MediaQuery.of(context).size.width,
                    height: 250,
                    child: Center(child: CircularProgressIndicator()));
              },
            ),
          ],
        ),
      ),
      floatingActionButton: _buildActionsButtons(),
    );
  }

  Widget _buildActionsButtons() {
    if (_selectedItemIndex < 0) {
      return (canAddInvoices)
          ? FloatingActionButton(
              onPressed: () {
                Navigator.pushReplacementNamed(
                  context,
                  AppRoutes.AddInvoiceScreenRoute,
                );
              },
              child: Icon(FontAwesomeIcons.plus),
              backgroundColor: AppTheme.primaryColor,
              heroTag: 'addInvoiceBtn',
            )
          : FloatingActionButton(
              onPressed: () {
                _alertDialog();
              },
              child: Icon(FontAwesomeIcons.question),
              backgroundColor: AppTheme.primaryColor,
              heroTag: 'questionBtn',
            );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              _userBloc.getUserFromCache().asStream().listen((user) async {
                String fullPath =
                    await _printService.saveAsPdf(_selectedItem, user);

                Navigator.pushReplacementNamed(context, '/pdf-preview',
                    arguments: {'pdf_path': fullPath});
              });
            },
            child: Icon(FontAwesomeIcons.paperPlane),
            backgroundColor: AppTheme.primaryColor,
            heroTag: 'sendInvoiceBtn',
          ),
          SizedBox(width: 16),
          FloatingActionButton(
            onPressed: () {
              Navigator.pushReplacementNamed(
                  context, AppRoutes.AddInvoiceScreenRoute,
                  arguments: {'invoice_model': _selectedItem});
            },
            child: Icon(FontAwesomeIcons.copy),
            backgroundColor: AppTheme.primaryColor,
            heroTag: 'copyInvoiceBtn',
          ),
          SizedBox(width: 16),
          FloatingActionButton(
            onPressed: deleteDialog,
            child: Icon(
              FontAwesomeIcons.trash,
            ),
            backgroundColor: AppTheme.secondaryColor,
            heroTag: 'deleteInvoiceBtn',
          )
        ],
      );
    }
  }

  deleteDialog() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Elimina Fattura'),
            content: Text('Vuoi eliminare questa fattura?'),
            actions: [
              FlatButton(
                child: Text('Si'),
                onPressed: () {
                  _invoicesBloc.delete(_selectedItem);
                  Navigator.of(context).pop();
                  setState(() {
                    _selectedItem = null;
                    _selectedItemIndex = -1;
                  });
                },
              ),
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  Widget _buildInvoiceBox(InvoiceModel invoice, int index) {
    return InkWell(
      onTap: () {
        setState(() {
          if (_selectedItemIndex < 0 || _selectedItemIndex != index) {
            _selectedItemIndex = index;
            _selectedItem = invoice;
          } else {
            _selectedItem = null;
            _selectedItemIndex = -1;
          }
        });
      },
      child: Container(
        margin: const EdgeInsets.all(10.0),
        padding: const EdgeInsets.all(15.0),
        decoration: _boxDecoration(index),
        child: _buildInvoiceBoxContent(invoice, index),
      ),
    );
  }

  BoxDecoration _boxDecoration(int index) {
    return BoxDecoration(
      color: AppTheme.bgColor,
      boxShadow: [
        BoxShadow(
          color: (_selectedItemIndex == index)
              ? AppTheme.secondaryColor.withOpacity(0.4)
              : Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3), // changes position of shadow
        ),
      ],
      border: Border.all(
          width: 1.0,
          color: (_selectedItemIndex == index)
              ? Colors.grey[400]
              : Colors.grey[200]),
      borderRadius: BorderRadius.all(
          Radius.circular(5.0) //         <--- border radius here
          ),
    );
  }

  Widget _buildInvoiceBoxContent(InvoiceModel invoice, int index) {
    DateFormat formatter = DateFormat('dd/MM/yyyy');
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 32.0, 16.0),
            child: (_selectedItemIndex == index)
                ? Icon(
                    FontAwesomeIcons.fileAlt,
                    color: AppTheme.secondaryTextColor,
                  )
                : Icon(
                    FontAwesomeIcons.fileAlt,
                    color: AppTheme.tertiaryTextColor,
                  ),
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1.0),
                  ),
                ),
                margin: EdgeInsets.only(bottom: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                      child: Text('Parcella n° ${invoice.number}'),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                      child: Text(formatter.format(invoice.timestamp)),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                    child: Text(invoice.recipient.fullname),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                    child: Text('€ ${invoice.amountAT.toStringAsFixed(2)}'),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  void _alertDialog() {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Row(
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            color: AppTheme.secondaryTextColor,
          ),
          SizedBox(
            width: 10,
          ),
          Text("Attenzione!"),
        ],
      ),
      content: Container(
        height: 125,
        child: Column(
          children: [
            Text(
                "Prima di procedere con l'inserimento delle fatture inserire i propri dati."),
            RichText(
              maxLines: 2,
              text: TextSpan(
                children: [
                  TextSpan(
                    style: AppTheme.alertTextStyle,
                    text: "(icona utente ",
                  ),
                  WidgetSpan(
                    child: Icon(
                      FontAwesomeIcons.user,
                      size: 16,
                    ),
                  ),
                  TextSpan(
                    style: AppTheme.alertTextStyle,
                    text: " nella barra in alto a destra). ",
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
