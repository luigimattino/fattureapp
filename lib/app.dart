import 'package:flutter/material.dart';
import 'app_theme.dart';
import 'app_routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Fatture',
      onGenerateRoute: AppRoutes.routes,
      theme: AppTheme.theme,
    );
  }
}
