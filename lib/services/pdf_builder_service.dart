import '../models/user_model.dart';
import '../models/invoice_model.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'dart:io';

class PdfBuilderService {
  var pdf;

  PdfBuilderService();

  _writeOnPdf(InvoiceModel invoice, UserModel user) {
    DateFormat formatter = DateFormat('dd/MM/yyyy');
    DateFormat yearFormatter = DateFormat('yy');
    pdf = pw.Document();
    bool isStampDutyMinimumLimit =
        (invoice.amountBT > InvoiceModel.stampDutyMinimumLimit);

    var rows = <pw.TableRow>[];
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.Text(
              'PARCELLA N° ${invoice.number}/${yearFormatter.format(invoice.timestamp)}'),
        ),
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topRight,
          height: 20,
          child: pw.Text(formatter.format(invoice.timestamp)),
        ),
      ],
    ));
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.SizedBox(height: 20),
        ),
        pw.Container(
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.SizedBox(height: 20),
        ),
      ],
    ));
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topCenter,
          height: 20,
          child: pw.Text('Descrizione della prestazione',
              style: pw.TextStyle(
                fontWeight: pw.FontWeight.bold,
              )),
        ),
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topCenter,
          height: 20,
          child: pw.Text('Importi',
              style: pw.TextStyle(
                fontWeight: pw.FontWeight.bold,
              )),
        ),
      ],
    ));
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.SizedBox(height: 20),
        ),
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topRight,
          height: 20,
          child: pw.Text("Euro"),
        ),
      ],
    ));
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.Text('${invoice.itemLabel}'),
        ),
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topRight,
          height: 20,
          child: pw.Text("Euro ${invoice.amountBT}"),
        ),
      ],
    ));
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.Text('Cassa di previdenza 2%'),
        ),
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topRight,
          height: 20,
          child: pw.Text("Euro ${invoice.taxAmount}"),
        ),
      ],
    ));
    if (isStampDutyMinimumLimit) {
      rows.add(pw.TableRow(
        children: [
          pw.Container(
            padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
            alignment: pw.Alignment.topLeft,
            height: 20,
            child: pw.Text("L'imposta di bollo"),
          ),
          pw.Container(
            padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
            alignment: pw.Alignment.topRight,
            height: 20,
            child: pw.Text(" Euro 2.0"),
          ),
        ],
      ));
    }
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.SizedBox(height: 20),
        ),
        pw.Container(
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.SizedBox(height: 20),
        ),
      ],
    ));
    rows.add(pw.TableRow(
      children: [
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topLeft,
          height: 20,
          child: pw.Text('Totale pagare',
              style: pw.TextStyle(
                fontWeight: pw.FontWeight.bold,
              )),
        ),
        pw.Container(
          padding: pw.EdgeInsets.fromLTRB(10, 0, 10, 0),
          alignment: pw.Alignment.topRight,
          height: 20,
          child: pw.Text("Euro ${invoice.amountAT}",
              style: pw.TextStyle(
                fontWeight: pw.FontWeight.bold,
              )),
        ),
      ],
    ));
    pdf.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        margin: pw.EdgeInsets.all(35),
        build: (pw.Context context) {
          return <pw.Widget>[
            pw.Row(
              mainAxisSize: pw.MainAxisSize.max,
              mainAxisAlignment: pw.MainAxisAlignment.start,
              children: [
                pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  mainAxisSize: pw.MainAxisSize.min,
                  children: <pw.Widget>[
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text(
                          "${user.sex == 'M' ? 'Dott.' : 'Dott.ssa'} ${user.fullName}"),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text(
                          "${user.firstRowAddress} ${user.secondRowAddress}"),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text("P.IVA ${user.partitaIvaNumber}"),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text("C.F. ${user.fiscalCodeNumber}"),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text(
                          "N° iscrizione Albo Psic. :${user.alboNumber}"),
                    ),
                  ],
                ),
              ],
            ),
            pw.Row(
              mainAxisSize: pw.MainAxisSize.max,
              mainAxisAlignment: pw.MainAxisAlignment.end,
              children: [
                pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.end,
                  mainAxisSize: pw.MainAxisSize.min,
                  children: <pw.Widget>[
                    pw.Align(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text(getHeadingByGenre(invoice.recipient.genre),
                          style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold,
                          )),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text(invoice.recipient.fullname,
                          style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold,
                          )),
                    ),
                    pw.SizedBox(height: 10),
                    pw.Align(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text(invoice.recipient.firstRowAddress,
                          style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold,
                          )),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerRight,
                      child: pw.Text(invoice.recipient.secondRowAddress,
                          style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold,
                          )),
                    ),
                    pw.SizedBox(height: 10),
                    pw.Align(
                      alignment: pw.Alignment.centerRight,
                      child:
                          pw.Text("C.F.: ${invoice.recipient.fiscalCodeNumber}",
                              style: pw.TextStyle(
                                fontWeight: pw.FontWeight.bold,
                              )),
                    ),
                  ],
                ),
              ],
            ),
            pw.SizedBox(height: 40),
            pw.Container(
              padding: pw.EdgeInsets.all(20),
              margin: pw.EdgeInsets.all(10),
              child: pw.Table(
                border: pw.TableBorder(),
                children: rows,
              ),
            ),
            pw.SizedBox(height: 40),
            pw.Row(
              mainAxisSize: pw.MainAxisSize.max,
              mainAxisAlignment: pw.MainAxisAlignment.center,
              children: [
                pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.center,
                  mainAxisSize: pw.MainAxisSize.min,
                  children: <pw.Widget>[
                    pw.Align(
                      alignment: pw.Alignment.center,
                      child: pw.Text(
                          "Operazione effettuata ai sensi art. 1 (commi da 54 a 89) Legge 190/2014",
                          style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold,
                          )),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.center,
                      child: pw.Text("(Regime Forfettario)",
                          style: pw.TextStyle(
                            fontWeight: pw.FontWeight.bold,
                          )),
                    ),
                  ],
                ),
              ],
            ),
            pw.SizedBox(height: 80),
            pw.Row(
              mainAxisSize: pw.MainAxisSize.max,
              mainAxisAlignment: pw.MainAxisAlignment.start,
              children: [
                pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  mainAxisSize: pw.MainAxisSize.min,
                  children: <pw.Widget>[
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text("Per Bonifico Bancario:"),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text(user.bankName),
                    ),
                    pw.Align(
                      alignment: pw.Alignment.centerLeft,
                      child: pw.Text(user.bankIban),
                    ),
                  ],
                ),
              ],
            ),
          ];
        },
      ),
    );
  }

  String getHeadingByGenre(String genre) {
    if (genre == 'M') {
      return "Al Sig.";
    } else if (genre == 'F') {
      return "Alla Sig.ra";
    } else if (genre == 'G') {
      return "Ai Genitori di";
    } else {
      return "A";
    }
  }

  Future<String> saveAsPdf(InvoiceModel invoice, UserModel user) async {
    _writeOnPdf(invoice, user);

    Directory documentDirectory = await getTemporaryDirectory();

    String documentPath = documentDirectory.path;
    String fullPath = "$documentPath/example.pdf";

    File file = File(fullPath);

    file.writeAsBytesSync(pdf.save());

    return fullPath;
  }
}
