import 'package:rxdart/rxdart.dart';
import '../models/user_model.dart';
import '../repository/local_storage_repository.dart';
import '../services/user_service.dart';

class UserBloc {
  BehaviorSubject<UserModel> _subjectUser;
  UserService _userService;

  UserBloc() {
    _userService = UserService(
        localStorageRepository: LocalStorageRepository("user.json"));
    _subjectUser = new BehaviorSubject<UserModel>();
  }

  ValueStream<UserModel> get userObservable => _subjectUser.stream;

  void fetchData() {
    _userService.getUserFromCache().asStream().listen((data) {
      _subjectUser.sink.add(data);
    });
  }

  Future<UserModel> getUserFromCache() => _userService.getUserFromCache();

  void saveData(UserModel data) {
    _userService.saveUser(data);
  }

  void deleteData() {
    _userService.deleteUser();
  }

  void dispose() {
    _subjectUser.close();
  }
}
